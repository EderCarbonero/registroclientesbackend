package com.eder.eventsequor;

import javax.persistence.EntityManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RegistroClientesApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistroClientesApplication.class, args);		
	}
	
	

}
