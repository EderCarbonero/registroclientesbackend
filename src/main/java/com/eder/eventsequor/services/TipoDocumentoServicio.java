package com.eder.eventsequor.services;

import java.util.List;

import com.eder.eventsequor.entities.TipoDocumento;

public interface TipoDocumentoServicio {
	List<TipoDocumento>listar();
	TipoDocumento listarId(long id);
	TipoDocumento add(TipoDocumento c);
	TipoDocumento edit(TipoDocumento c);
	TipoDocumento delete(long id);
}
