package com.eder.eventsequor.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eder.eventsequor.entities.Cliente;
import com.eder.eventsequor.repository.ClienteRepositorio;

@Service
public class ClienteServicioImp implements ClienteServicio{

	@Autowired
	private ClienteRepositorio repositorio;
	
	@Override
	public List<Cliente> listar() {
		return repositorio.findAll();
	}

	@Override
	public Cliente listarId(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cliente add(Cliente c) {
		return repositorio.save(c);
	}

	
	
	@Override
	public Cliente edit(Cliente c) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cliente delete(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cliente findBy(long id) {		
		return repositorio.findById(id);
	}

}
