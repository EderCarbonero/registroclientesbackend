package com.eder.eventsequor.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eder.eventsequor.entities.TipoDocumento;
import com.eder.eventsequor.repository.TipoDocumentoRepositorio;

@Service
public class TipoDocumentoServicioImp implements TipoDocumentoServicio{

	@Autowired
	private TipoDocumentoRepositorio repositorio;
	
	@Override
	public List<TipoDocumento> listar() {
		return repositorio.findAll();
	}

	@Override
	public TipoDocumento listarId(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoDocumento add(TipoDocumento c) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoDocumento edit(TipoDocumento c) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoDocumento delete(long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
