package com.eder.eventsequor.services;

import java.util.List;

import org.springframework.stereotype.Component;

import com.eder.eventsequor.entities.Cliente;

public interface ClienteServicio {
	List<Cliente>listar();
	Cliente listarId(long id);
	Cliente findBy(long id);
	Cliente add(Cliente c);
	Cliente edit(Cliente c);
	Cliente delete(long id);
}
