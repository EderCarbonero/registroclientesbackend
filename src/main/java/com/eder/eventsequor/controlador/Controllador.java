package com.eder.eventsequor.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eder.eventsequor.entities.Cliente;
import com.eder.eventsequor.entities.TipoDocumento;
import com.eder.eventsequor.services.ClienteServicio;
import com.eder.eventsequor.services.TipoDocumentoServicio;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
//@RequestMapping(value = "/clientes")
public class Controllador {

	@Autowired
	ClienteServicio servicio;

	@Autowired
	TipoDocumentoServicio tipoServio;

	@GetMapping("/cliente")
	public List<Cliente> listar() {
		return servicio.listar();
	}

	@PostMapping("/guardar")
	public Cliente guardar(@RequestBody Cliente c) {
		System.out.println(">>>>>>> Celular " + c.getCelular());
		System.out.println(">>>>>>> Cedula " + c.getIdCedula());
		System.out.println(">>>>>>> correo " + c.getCorreo());
		System.out.println(">>>>>>> Tipo documento " + c.getTipo().getTipoDocumento());
		System.out.println(">>>>>>> Tipo id " + c.getTipo().getId());
		try {
			System.out.println(">>>>>>>>> ### "+servicio.findBy(700000).getCorreo());
		} catch (Exception e) {
			System.out.println("Error al consultar los datos");
		}
		
		return servicio.add(c);
	}

	@GetMapping("/tipo")
	public List<TipoDocumento> listarTipo() {
		return tipoServio.listar();
	}

}
