package com.eder.eventsequor.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_documento")
public class TipoDocumento {
	
	@Id
	@GeneratedValue( strategy=GenerationType.AUTO )
	@Column(name = "id",length = 10)
	private long id;
	
	@Column(name = "tipo_documento",length = 50)
	private String tipoDocumento;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumneto) {
		this.tipoDocumento = tipoDocumneto;
	}

	

}
