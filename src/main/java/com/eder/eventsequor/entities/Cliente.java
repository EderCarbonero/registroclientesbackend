package com.eder.eventsequor.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.ForeignKey;

import javax.persistence.*;

@Entity

@Table(name = "clientes")
public class Cliente {

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idCedula;
	
	@Column (name = "correo",length = 50)
	private String correo;
//	private String tipo_documento;
	
	@Column (name = "celular",length = 50)
	private String celular;
	
	@ManyToOne
	
	@JoinColumn(name = "tipo_documento")
	private TipoDocumento tipo;

	

	public Cliente() {
		super();
	}

	public Cliente(long idCedula, String correo, String celular, TipoDocumento tipo) {
		super();
		this.idCedula = idCedula;
		this.correo = correo;
		this.celular = celular;
		this.tipo = tipo;
	}
	
	public long getIdCedula() {
		return idCedula;
	}

	public void setIdCedula(long idCedula) {
		this.idCedula = idCedula;
	}

	
	
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public TipoDocumento getTipo() {
		return tipo;
	}

	public void setTipo(TipoDocumento tipo) {
		this.tipo = tipo;
	}

}
