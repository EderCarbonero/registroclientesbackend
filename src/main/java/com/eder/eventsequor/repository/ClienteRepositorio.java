package com.eder.eventsequor.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.eder.eventsequor.entities.Cliente;

public interface ClienteRepositorio extends Repository<Cliente, Long>{
	List<Cliente>findAll();
	Cliente save(Cliente p);
	Cliente findById(long id);
	void delete(Cliente c);
}
