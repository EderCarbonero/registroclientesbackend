package com.eder.eventsequor.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.eder.eventsequor.entities.TipoDocumento;


public interface TipoDocumentoRepositorio extends Repository<TipoDocumento, Long>{
	List<TipoDocumento>findAll();
	TipoDocumento save(TipoDocumento p);
	void delete(TipoDocumento c);

}
